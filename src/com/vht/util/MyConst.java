package com.vht.util;

import com.vht.Configuration;

public class MyConst {

    private static Configuration config = Configuration.getInstance();
    public static final String SMS_API_SECRET	= config.getProperties().getString("sms.apisecret");
    public static final String SMS_API_KEY		= config.getProperties().getString("sms.apikey");
    public static final String SMS_BRANDNAME	= config.getProperties().getString("sms.brandname");
    public static final String SMS_PUBLICURL	= config.getProperties().getString("sms.publicurl");

    
}

