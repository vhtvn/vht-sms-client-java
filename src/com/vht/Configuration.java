package com.vht;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

public final class Configuration {

	private static final String PROPERTIES_FILE_NAME = "configuration.properties";
	private static final Configuration INSTANCE = new Configuration();
	private final PropertiesConfiguration properties = new PropertiesConfiguration();

	public static Configuration getInstance() {
		return INSTANCE;
	}

	private Configuration() {
		try {
			this.properties.load(PROPERTIES_FILE_NAME);
		} catch (ConfigurationException e) {
			e.printStackTrace();
			System.exit(1);
		}

		FileChangedReloadingStrategy reloadingStrategy = new FileChangedReloadingStrategy();
		reloadingStrategy.setRefreshDelay(5000);
		this.properties.setReloadingStrategy(reloadingStrategy);
	}

	public PropertiesConfiguration getProperties() {
		return this.properties;
	}

}