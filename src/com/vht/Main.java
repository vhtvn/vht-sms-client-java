package com.vht;

import java.net.URL;

import com.vht.util.MyConst;
import com.vht.util.SSLExcludeCipherConnectionHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Main {

    private static Log LOGGER = LogFactory.getLog(Main.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        String phone = "0981660168";
        String message = "Xin chao, day la tin nhan duoc gui ra tu he thong VHT";
        try {

            URL url = new URL(MyConst.SMS_PUBLICURL);
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.append("{\"submission\":{\"api_key\":\"");
            strBuilder.append(MyConst.SMS_API_KEY);
            strBuilder.append("\",\"api_secret\":\"");
            strBuilder.append(MyConst.SMS_API_SECRET);
            strBuilder.append("\",\"sms\":[{\"brandname\":\"");
            strBuilder.append(MyConst.SMS_BRANDNAME);
            strBuilder.append("\",\"text\":\"");
            strBuilder.append(message);
            strBuilder.append("\",\"to\":\"");
            strBuilder.append(phone);
            strBuilder.append("\"}]}}");

            LOGGER.info(strBuilder.toString());

            javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() {
                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        return hostname.equals("sms.vht.com.vn");
                    }
                }
            );

            SSLExcludeCipherConnectionHelper sslExclHelper = new SSLExcludeCipherConnectionHelper(System.getProperty("user.dir") + "/sms.server.cert");
            sslExclHelper.post(url, strBuilder.toString());

            LOGGER.info("Success");

        } catch (Exception ex) {
            ex.printStackTrace();

            LOGGER.error(ex.getMessage());
        }
	}
}
